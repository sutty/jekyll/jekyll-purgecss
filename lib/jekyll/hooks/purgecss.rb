# frozen_string_literal: true

require 'open3'
require 'digest'

class PurgecssNotFoundError < RuntimeError; end
class PurgecssRuntimeError < RuntimeError; end

Jekyll::Hooks.register(:site, :post_write, priority: 21) do |site|
  next unless Jekyll.env == 'production'

  raise PurgecssNotFoundError unless File.file?('./node_modules/.bin/purgecss')

  PURGECSS = File.join('node_modules', '.bin', 'purgecss')

  css_files = site.config.dig('purgecss', 'css_files') || []
  allowlist = site.config.dig('purgecss', 'allowlist') || []

  unless allowlist.is_a?(Array)
    Jekyll.logger.error 'PurgeCSS:', 'purgecss.allowlist must be array'
    allowlist = []
  end

  unless css_files.is_a?(Array)
    Jekyll.logger.error 'PurgeCSS:', 'purgecss.css_files must be array'
    css_files = []
  end

  css_files.each do |css|
    digest_filename = Digest::SHA256.hexdigest([css].concat(allowlist.sort).join)
    digest_file     = File.join(site.cache_dir, digest_filename)
    purged_file     = "#{digest_file}.css"
    current_digest  = Digest::SHA256.file(css).hexdigest
    previous_digest = File.read(digest_file) if File.exist?(digest_file)
    absolute_css    = site.in_dest_dir(css)

    if current_digest == previous_digest
      Jekyll.logger.info 'PurgeCSS:', "Already purged #{css}, recovering from cache"
      FileUtils.rm_f(absolute_css)
      FileUtils.cp(purged_file, absolute_css)
    else
      Jekyll.logger.info 'PurgeCSS:', "Purging #{css}"

      cmd = [
        PURGECSS,
        '--css', absolute_css,
        '--content', File.join(site.dest, '**', '*.html'), File.join(site.dest, '*.html'),
        '--out', File.dirname(absolute_css)
      ]

      unless allowlist.empty?
        cmd << '-w'
        cmd.concat(allowlist)
      end

      Open3.popen2e(*cmd, chdir: site.source) do |_, o, t|
        Thread.new do
          o.each do |line|
            Jekyll.logger.info 'PurgeCSS:', line
          end
        rescue IOError => e
          Jekyll.logger.error 'PurgeCSS:', e.message
        end

        raise PurgecssRuntimeError unless t.value.success?
      end

      FileUtils.rm_f(purged_file)
      FileUtils.cp(absolute_css, purged_file)
      File.write(digest_file, current_digest)
    end
  end
end
